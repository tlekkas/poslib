#!/usr/local/bin/python
from __future__ import division


import math


from decimal import *



def equity_size():
	global equi
	print "Please enter the value of your portfolio."
	equi = raw_input("> ")
	return Decimal(equi)


def max_risk():
	global ma_ri
	print "Please enter your risk threshold."
	print "For daytrades the risk threshold is capped at 3%."
	ma_ri = Decimal(raw_input("> ")) / 100
	
	if ma_ri > Decimal(.03):
		print "The max risk threshold is too high."
		print "Adjust this risk to 3% or lower."
		return max_risk()
		
	else:
		return Decimal(ma_ri)

		
def price_entry():
	global buy_price
	print "Please enter your price entry."
	buy_price = raw_input("> ")
	return Decimal(buy_price)

	
def pre_determined_stop():
	global pre_stop
	print "Please enter your stop: "
	pre_stop = raw_input("> ")
	return Decimal(pre_stop)

	
def trade_amount():
	global scalp_dollar_risk
	print "Please enter the amount you wish to allocate to this trade."
	scalp_dollar_risk = raw_input("> ")
	
	if Decimal(scalp_dollar_risk) > (Decimal(equi) * 4):
		print "You can not use more than 4x's leverage."
		print "Please enter a valid amount."
		return trade_amount()
	
	elif Decimal(scalp_dollar_risk )<= 0:
		print "Please enter a valid amount."
	
		return trade_amount()
	else:
		return Decimal(scalp_dollar_risk)


#This calculation is used when there is no pre-determined stop.
def shares_to_buy():
	shares_calc = (Decimal(equi) * Decimal(ma_ri))/(
								Decimal(buy_price) - Decimal(generated_stop))
	print Decimal(shares_calc)


#This calculation is used when there is a pre-determined stop.
def shares_buying():
	share_calcu = (Decimal(equi) * Decimal(ma_ri))/(
									Decimal(buy_price) - Decimal(pre_stop))
	print Decimal(share_calcu)

	
def stop_to_use():
	global generated_stop
	print "This is your stop."
	generated_stop = (Decimal(buy_price) * (Decimal(scalp_dollar_risk) - (
				Decimal(equi) * Decimal(ma_ri)))) / Decimal(scalp_dollar_risk)
	print generated_stop


def start():
	print """
	We need some information before we can return the number of 
	shares to purchase.
	"""
	equity_size()
	max_risk()
	price_entry()
	trade_amount()
	
	print "Do you have a stop in mind? If not, we can provide one for you."
	print "Please answer yes or no."
	next = raw_input()
	
	if next[:1] == 'y':
		pre_determined_stop()
		return shares_buying()

	else:
		stop_to_use()
		return shares_to_buy()

		
start()