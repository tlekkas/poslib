import dateutil
import urllib
import n_calculator


from datetime import date
from decimal import *


def x_2_product(n):
	levered2 = ['FSU', 'FSE', 'FSG', 'FSA', 'FOL', 'DAG', 'AGA', 'BDD', 'BOM',
				'DYY', 'DEE', 'OLO', 'SZO', 'DGP', 'DZZ', 'TBZ', 'UST', 'PST',
				'UBT', 'TBT', 'GDAY', 'CROC', 'UYM', 'SMN', 'UGE', 'SZK',
				'SCC', 'UCD', 'CMD', 'UCO', 'SCO', 'BOIL', 'KOLD', 'DDM',
				'ULE', 'EUO', 'UYG', 'SKF', 'XPP', 'FXP', 'UGL', 'GLL', 'RXL',
				'RXD', 'UJB', 'UXI', 'SIJ', 'IGU', 'KRU', 'KRS', 'MVV', 'MZZ',
				'UBR', 'BZQ', 'EFO', 'EFU', 'EET', 'EEV', 'UPV', 'EPV', 'EZJ',
				'EWV', 'UMX', 'SMK', 'BIB', 'BIS', 'DIG', 'DUG', 'UXJ', 'JPX',
				'QLD', 'QID', 'URE', 'SRS', 'UKW', 'SDK', 'UVU', 'SJL', 'UKF',
				'SFK', 'UVG', 'SJF', 'UWM', 'TWM', 'UKK', 'SKK', 'UVT', 'SJH',
				'UWC', 'TWQ', 'SSO', 'SDS', 'USD', 'SSG', 'AGQ', 'ZSL', 'SAA',
				'SDD', 'ROM', 'REW', 'LTL', 'TLL', 'TPS', 'UPW', 'SDP',
				'YCL', 'YCS', 'RSU', 'RSW', 'DVYL', 'SDYL', 'EIPL', 'BDCL',
				'LCPR', 'SCPR', 'TVIX', 'TVIZ', 'LPLT', 'IPLT', 'LPAL',
				'UCC', 'DXD', 'UVXY', 'IPAL']

	if n in levered2:
		vap_2 = (vap / 2)
		print round(vap_2, 0)
	else:
		print round(vap, 0)


def x_3_product(n):
	levered3 = ['COWL', 'COWS', 'FAS', 'FAZ', 'MIDU', 'MIDZ', 'TNA', 'TZA',
				'ERY', 'SPXL', 'SPXS', 'TECL', 'TECS', 'EDC', 'EDZ', 'DZK',
				'DRN', 'DRV', 'MATL', 'MATS', 'BRIL', 'BRIS', 'YINN', 'YANG',
				'NUGT', 'DUST', 'CURE', 'SICK', 'INDL', 'INDZ', 'LBJ', 'LHB',
				'GASL', 'GASX', 'RETL', 'RETS', 'RUSL', 'RUSS', 'SOXL',
				'ERX', 'DPK', 'TYD', 'TYO', 'TMF', 'TMV', 'LBND', 'SBND',
				'JGBD', 'ITLT', 'JGBT', 'UDNT', 'UUPT', 'TTT', 'UPRO', 'SPXU',
				'FINU', 'FINZ', 'TQQQ', 'SQQQ', 'UDOW', 'SDOW', 'UMDD',
				'URTY', 'SRTY', 'UOIL', 'DOIL', 'UWTI', 'DWTI', 'UGLD',
				'UGAZ', 'DGAZ', 'USLV', 'DSLV', 'BUNT', 'SOXS', 'SMDD', 
				'DGLD',]

	if n in levered3:
		vap_3 = (vap / 3)
		print round(vap_3, 0)
	else:
		x_2_product(n)


def vap_unit(symbol):
	global vap
	vap = ((Decimal(ma_ri) * Decimal(equi))/vol) * Decimal(.16667)
	x_3_product(symbol)
	return round(vap, 0)


def volatility():
	global vol
	vol = val_n * Decimal(lever)
	return vol


def equity_size(portfolio):
	# print "Please enter the value of your portfolio."
	global equi
	equi = portfolio
	return Decimal(equi)


def max_risk(risk):
	global ma_ri
	# print "Please enter your risk threshold."
	# print "For positional trades, risk threshold is capped at 2%."
	# print "Please enter in decimal, .01, format."
	ma_ri = risk
	
	if ma_ri > .02:
		# print "The max risk threshold is too high."
		# print "Adjust this risk to 2% or lower."
		return max_risk()
		
	else:
		return Decimal(ma_ri)


def dollar_unit(leverage):
	# print "Please enter the leverage of your instrument."
	# print "Ex.: Stock = 1, Options = 100, etc."
	# print "Note: Futures are not yet supported."
	global lever
	lever = leverage
	return Decimal(lever)

def position(symbol, date, portfolio, risk, leverage):
	global val_n
	
	val_n = n_calculator.average_atr(symbol, date)
	equity_size(portfolio)
	max_risk(risk)
	dollar_unit(leverage)
	volatility()
	return vap_unit(symbol)