import math
import datetime
import ystockquote


from decimal import *


def split_check(n):
	# if initial_li[-1] != initial_li[4]:
	# 	print "There was a recent split and that calculation is not currently supported."
	# else:
	pass	
	
def new_time(n):
	fmt = "%Y%m%d"
	end_date1 	= datetime.datetime.strptime(n, fmt)
	sixty_day 	= datetime.timedelta(days=32)
	start_date 	= end_date1 - sixty_day
	start_date1 = str(start_date)
	start_date2 = start_date1[:4] + start_date1[5:7] + start_date1[8:10]
	return start_date2


def average_atr(symbol, date):
	symbol = str.upper(symbol)
	# vol_check(symbol)
	# symbol_check(symbol)

	# print "Enter the end date in (YYYYMMDD) format: "
	end_date 		= date
	start_date 		= new_time(end_date)
	initial_li  	= ystockquote.get_historical_prices(symbol, start_date, end_date)

	# split_check(initial_list)

	initial_adj 	= initial_li[1:]
	table_adj		= [[r[0], Decimal(r[1]), Decimal(r[2]), Decimal(r[3]), 
						Decimal(r[4]), r[5], Decimal(r[6])] for r in initial_adj]

	#Used specifically for calculating the Previous Day Close (PDC).
	pdc_li			= table_adj[1:]

	#High - Low.
	h_l = [r[2] - r[3] for r in table_adj]
	#High - PDC
	h_pdc	= [r[2] - j[6] for r, j in zip(table_adj, pdc_li)]
	#PDC - Low
	pdc_l	= [j[6] - r[3] for j, r in zip(pdc_li, table_adj)]
	#As of 2012-08-10 Yahoo Finance is NOT generating consistent data.  The
	#issue stems from a sync problem between different servers.  There is no
	#ETA on a resolution of the problem.  This will have some affect on the
	#output generated by the sizing algorithm.

	true_range 	= map(lambda x,y,z:max(x, y, z), h_l, h_pdc, pdc_l)
	twenty_ave	= sum(true_range[1:-1])/len(true_range[1:-1])
	n_unit		= (19*twenty_ave+true_range[0])/20
	return n_unit