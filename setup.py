#!/usr/bin/env python

from distutils.core import setup

setup(name='PosLib',
	version='.0.2.4',
	description='A position sizing "package"',
	author='Theo Lekkas',
	author_email='theofanis.lekkas@theofanislekkas.com',
	py_modules=['Basic']
	)
